USE [impresario]
GO
/****** Object:  StoredProcedure [dbo].[LP_DONOR_HEADER_DEV]    Script Date: 5/17/2019 1:55:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[LP_DONOR_HEADER_DEV](
@CUSTOMER_NO INT 
) with recompile
AS SET NOCOUNT ON
/**********************************************************************************************************
Author : Cindy Emig Date : March 15, 2003
This procedure is for donors module of Impresario system.
It provides the header information for the Development Donors screen in the donors module.
MODIFIED for sql2005 upgrade (on account amt) 4/5/2007
cm 6/15/2011 added code to display 'see csi' in header when an open attention csi exists
jhirst 5/8/2012 Updated for v11 structure 
exec lp_donor_header_dev 100011
exec lp_donor_header_dev 1113773
exec lp_donor_header_dev 160670
exec lp_donor_header_dev 169674
exec lp_donor_header_dev 279936
select * FROM T_CUSTOMER WHERE FNAME like 'SCOT%' AND LNAME LIKE '%za'
dhe /5/4/2017 Commented out the existing donation statements and added the code from 12 Month Giving#12999
**************************************************************************************************************/
IF ISNULL(@customer_no, 0) = 0
BEGIN
select customer_no = @customer_no, 
full_name1 = ' ',
full_name2 = ' ', 
all_const = ' ', 
memb_level = ' ', 
current_status = ' ',
memb_expirtion = convert(datetime, null), 
create_dt = convert(datetime, null),  donation = 0, 
userid = [dbo].[fs_user](), 
batch_no = null,
on_account = 0,
inactive = '',
batch_type = null,
designation = null,
association = null,
matching_gifts = null,
o_series = null,
decline_benefits = null,
event_payment = 0,
fee_ind = NULL,
bcr_rep = ' ',
bad_add_ind = null,
see_note = '',
primary_manager = null,
ask_amt = 0,
hot_prospect = null,
no_solicitation = null,
no_invite = null,
next_perf = null
RETURN
END
DECLARE @constituency varchar(3)
DECLARE @all_const varchar(255)
DECLARE @full_name1 varchar(80)
DECLARE @full_name2 varchar(80)
DECLARE @memb_level varchar(3)
DECLARE @current_status varchar(30) DECLARE @expiration datetime
DECLARE @create_dt datetime
DECLARE @donation money
DECLARE @start_dt datetime
DECLARE @end_dt datetime
DECLARE @on_account money
DECLARE @inactive int
DECLARE @inactive_reason int
DECLARE @done int
DECLARE @cust_Memb_no int
DECLARE @show_highest_level char(1)
DECLARE @designation char(1)
DECLARE @association char(1)
DECLARE @matching_gifts char(1)
DECLARE @o_series char(8)
DECLARE @decline_benefits char(1)
DECLARE @event_payment money
DECLARE @fee_ind char(1)
DECLARE @csi_ind char(1)
DECLARE @bcr_rep varchar(15)
DECLARE @bad_add_ind int
DECLARE @dnr_desig varchar(30)
declare @see_note char(1)
declare @primary_manager varchar(250) 
declare @ask_amt money
declare @hot_prospect int
declare @no_solicitation varchar(20)
declare @no_invite varchar(20)
declare @next_perf varchar(30), 
		@next_event varchar(50)
create table #work1(esal1_desc char(55) null, esal2_desc char(55) null, lsal_desc char(55) null)
begin
/**********************************************/ 
/* Get all the constituencies and concatenate */
/**********************************************/
--EXEC AP_CONST_STRING @customer_no, @all_const OUTPUT
--set @all_const = (select dbo.FS_CONST_STRING_NEW (@customer_no, 'Y' )) 
set @all_const = (select [dbo].[LFS_CONST_STRING_AFFILIATE] (@customer_no, 'Y'))
/********************************************************************/
/* Get the full_name1, full_name2 of the customer and concatenate  */
/********************************************************************/
SELECT @create_dt = a.create_dt,
@inactive = a.inactive,
@inactive_reason = a.inactive_reason 
FROM t_customer a (NOLOCK)
where customer_no = @customer_no



If Exists (Select * from tr_salutation_format Where id = 0)
Begin
insert #work1 exec AP_BUILD_SAL @customer_no = @customer_no, @formula_id = 0
Select @full_name1 = esal1_desc, @full_name2 = esal2_desc
From #work1
Option (keep plan)
End
Else
SELECT @full_name1 = esal1_desc, @full_name2 = esal2_desc FROM VXS_CUST_SAL (NOLOCK)
WHERE customer_no = @customer_no and default_ind = 'Y'
/****************************************************************************/
/* Get the active (or pending if pending) membership level and its expiry date */ --remove pending per AMY 25May16
/****************************************************************************/ 
--groups, households, primary affiliates
select --@expiration = expr_dt,
@current_status = current_status_desc,
@memb_level = memb_level
from VS_AFFILIATION a
outer apply dbo.ft_membership_info(a.group_customer_no, null, 'N') b 
where a.primary_ind = 'Y'and b.current_status <> 3 
and a.individual_customer_no = @customer_no
--individuals
select --@expiration = expr_dt,
@current_status = current_status_desc,
@memb_level = memb_level
from dbo.ft_membership_info(@customer_no, null, 'N')
where current_status <> 3
--MS expiration
set @expiration = (select MAX( expr_dt) from VXS_CUST_MEMBERSHIP where customer_no = @CUSTOMER_NO and memb_org_no=1 
and current_status in (2,3,4,7))
--select * from TX_CUST_MEMBERSHIP
/*
/**********************************************/
/* Get contributions tied to current membership */
/**********************************************/
select @donation = 0
--primary affiliates
select @donation = sum(d.cont_amt)
from VS_AFFILIATION a
join VXS_CUST_MEMBERSHIP b on a.group_customer_NO = b.customer_no
join TX_CONT_MEMB c on b.cust_memb_no = c.cust_memb_no
join vs_contribution d on c.cont_ref_no = d.ref_no
where b.memb_org_no = 1
and b.cur_record = 'Y'
and a.primary_ind = 'Y'
and a.individual_customer_no = @customer_no
--individual, household, orgs
if @CUSTOMER_NO not in (select individual_CUSTOMER_no from VS_AFFILIATION where primary_ind = 'Y')
Begin
select @donation = sum(a.cont_amt)
--SUM(a.cont_amt)
from vs_contribution a
join TX_CONT_MEMB c on a.ref_no = c.cont_ref_no
join VXS_CUST_MEMBERSHIP d on c.cust_memb_no = d.cust_memb_no
where d.customer_no = @customer_no 
and d.memb_org_no = 1
and d.cur_record = 'Y'
*/
/**********************************************/
/* Get amt towards current membership + amount above membership level */
/**********************************************/
select @donation = 0
select @donation = (isnull(x.memb_amt,0) + isnull(x.AVC_amt,0))
from LV_MEMBERSHIP_CURRENT a
join tx_cust_membership x on a.cust_memb_no = x.cust_memb_no
where a.customer_no = @CUSTOMER_NO

/**********************************************/
/* Get contributions for last 12 months */
/**********************************************/
--added 5/4/18 
--primary affiliates
/*select @donation = sum(b.cont_amt)
from VS_AFFILIATION a
join vs_contribution b on a.group_customer_NO = b.customer_no
join VS_CAMPAIGN c on b.campaign_no = c.campaign_no
where ( c.memb_org_no = 1
and b.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and b.fund_no not in (179,182)
and (c.description like '%annual campaign%' or c.description like '%production_sponsorship%')
and a.primary_ind = 'Y'
and a.individual_customer_no = @customer_no)
or (b.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and b.fund_no not in (179,182)
and (b.campaign_no in (21701,21694,22369,21954))
and a.primary_ind = 'Y'
and a.individual_customer_no = @customer_no)
--primary affiliates
select @donation = isnull(@donation,0) + isnull(sum(c.credit_amt),0)
from VS_AFFILIATION a
join vs_contribution b on a.group_customer_NO = b.customer_no and b.cont_amt > 0
join t_creditee c on b.ref_no = c.ref_no
and c.creditee_no = a.group_customer_no
join VS_CAMPAIGN d on b.campaign_no = d.campaign_no
and d.memb_org_no = 1
where b.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and b.fund_no not in (179,182)
and (d.description like '%annual campaign%' or d.description like '%production_sponsorship%')
and a.primary_ind = 'Y'
and a.individual_customer_no = @customer_no
--individual, household, orgs
if @CUSTOMER_NO not in (select individual_CUSTOMER_no from VS_AFFILIATION where primary_ind = 'Y')
Begin
select @donation = sum(a.cont_amt)
from vs_contribution a
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
where a.customer_no = @customer_no 
and (b.memb_org_no = 1
and a.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and a.fund_no not in (179,182)
and (b.description like '%annual campaign%' or b.description like '%production_sponsorship%'))
or a.customer_No = @customer_no and (a.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and a.fund_no not in (179,182)
and (a.campaign_no in (21701,21694,22369,21954))) --RFI,Radio,Media,BNG
--individual, households, orgs
select @donation = isnull(@donation,0) + isnull(sum(b.credit_amt),0)
from vs_contribution a
join t_creditee b on a.ref_no = b.ref_no
and b.creditee_no = @customer_no
join VS_CAMPAIGN c on a.campaign_no = c.campaign_no
and c.memb_org_no = 1
where a.cont_dt  between  dateadd(mm, -12, getdate()) and getdate() 
and a.fund_no not in (179,182) 
and (c.description like '%annual campaign%' or c.description like '%production_sponsorship%')
and a.cont_amt > 0
End
*/
/**********************************************/
/*    Get the sum of on account money   */
/**********************************************/
Select @on_account = -1 * SUM(pmt_amt)
From [dbo].t_payment a WITH (NOLOCK INDEX=IND_t_payment_customer_no)
JOIN [dbo].vrs_payment_method b WITH (NOLOCK) ON a.pmt_method = b.id -- changed from VRS_PAYMENT_METHOD to vrs_payment_method CWR 1/6/2006
Where a.customer_no = @customer_no and
b.pmt_type = 2
/**********************************************/
/*    Get the customer's designation     */
/**********************************************/
select @designation = NULL
/*
--primary affiliates
Select @designation = 'X'
From VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.custom_4 = 1 --v12 replace cont_designation throughout
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
Select @designation = 'B'
From VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.custom_4 = 2
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
Select @designation = 'W'
From VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.custom_4 = 3
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
Select @designation = 'G'
From VS_AFFILIATION x 
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.custom_4 = 4
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
Select @designation = 'C'
From VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.custom_4  between  6 and 29
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
Select @designation = '!'
From VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where 
a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and x.primary_ind = 'Y'
and @CUSTOMER_NO = x.individual_customer_no
and ((@designation = 'X'
and a.custom_4 in (2,3,4,29))
or (@designation = 'B'
and a.custom_4 in (1,3,4,29))
or (@designation = 'W'
and a.custom_4 in (1,2,4,29))
or (@designation = 'G'
and a.custom_4 in (1,2,3,29))
or (@designation = 'C'
and a.custom_4 in (1,2,3,4)))
--individual, household, orgs
if @CUSTOMER_NO not in (select individual_CUSTOMER_no from VS_AFFILIATION where primary_ind = 'Y')
Begin
Select @designation = 'X'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no and
a.custom_4 = 1
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
Select @designation = 'B'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no and
a.custom_4 = 2
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
Select @designation = 'W'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no and
a.custom_4 = 3
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
Select @designation = 'G'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no and
a.custom_4 = 4
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
Select @designation = 'C'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no and
a.custom_4  between  6 and 29
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
Select @designation = '!'
From VS_CONTRIBUTION a 
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no
Where a.customer_no = @customer_no 
and a.cont_dt  between  dateadd(yy,-3,getdate()) and getdate()
and b.memb_org_no = 1
and ((@designation = 'X'
and a.custom_4 in (2,3,4,29))
or (@designation = 'B'
and a.custom_4 in (1,3,4,29))
or (@designation = 'W'
and a.custom_4 in (1,2,4,29))
or (@designation = 'G'
and a.custom_4 in (1,2,3,29))
or (@designation = 'C'
and a.custom_4 in (1,2,3,4)))
END
*/
/**********************************************/
/*    Identify any associations         */
/**********************************************/
/*select @association = 'N'
--Assuming that associations are on the individual level and not pushing household down to affiliates
--groups, households, individuals
select @association = 'Y'
from VS_ASSOCIATION a
where (a.customer_no = @CUSTOMER_NO
or a.associated_customer_no = @CUSTOMER_NO)
and a.association_type_id in (61,63,78,86,90) -- solicitee
and a.inactive = 'N'
*/
--Select @association = 'Y'
--From tx_xref a 
--Where (a.associate_no = @customer_no or 
-- a.customer_no = @customer_no)and
-- a.type in (63,64,65,66,67)
-- and a.inactive = 'N'
--xref no longer used in v11
/**********************************************/
/*    Identify any matching gifts        */
/**********************************************/
/*select @matching_gifts = 'N'
--groups, households,individuals
Select @matching_gifts = 'Y'
From t_creditee a 
Where a.creditee_no = @customer_no 
--primary affiliates
Select @matching_gifts = 'Y'
from VS_AFFILIATION x
join T_CREDITEE a on x.group_customer_no = a.creditee_no
where x.primary_ind = 'Y'
and (@CUSTOMER_NO = x.group_customer_no or @CUSTOMER_NO = x.individual_customer_no)
*/
/**********************************************/
/*    Identify O series patrons         */
/**********************************************/
--households, groups, individuals
Select @o_series = 'O Series'
From VS_PACKAGE_HISTORY a --v12
join VRS_SEASON b on a.season = b.id
Where a.customer_no = @customer_no 
and a.pkg_code = 'O' --v12
and getdate()  between  b.start_dt and b.end_dt
and b.type = 1
--primary affiliates
Select @o_series = 'O Series'
From VS_AFFILIATION x
join VS_PACKAGE_HISTORY a on x.group_customer_no = a.customer_no --v12
join VRS_SEASON b on a.season = b.id
Where x.primary_ind = 'Y'
and (@CUSTOMER_NO = x.group_customer_no or @CUSTOMER_NO = x.individual_customer_no)
and a.pkg_code = 'O' --v12
and getdate()  between  b.start_dt and b.end_dt
and b.type = 1
/**********************************************/
/*    Identify decline benefit patrons     */
/**********************************************/
select @decline_benefits = 'N'
--groups, households, individuals
/*Select @decline_benefits = 'Y'
From VXS_CUST_MEMBERSHIP a 
Where a.customer_no = @customer_no 
and a.memb_org_no = 1
and a.declined_ind = 'y'
and a.cur_record = 'y'
--primary affiliates
Select @decline_benefits = 'Y'
From VS_AFFILIATION x
join VXS_CUST_MEMBERSHIP a on x.group_customer_no = a.customer_no 
Where (@CUSTOMER_NO = x.group_customer_no or @CUSTOMER_NO = x.individual_customer_no)
and x.primary_ind = 'Y'
and a.memb_org_no = 1
and a.declined_ind = 'y'
and a.cur_record = 'y'
*/
/***************************************************/
/* Get the past fourteen month event payments  */
/**************************************************/
select @event_payment = 0
--individual, household, group
select @event_payment = sum(a.cont_amt)
from VS_CONTRIBUTION a
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no 
where a.customer_no = @customer_no 
and b.camp_type = 'e'
and a.cont_dt  between  dateadd(mm, -14, getdate()) and getdate() 
--primary affilites
if @CUSTOMER_NO in (select individual_customer_no from VS_AFFILIATION where primary_ind = 'Y')
BEGIN
select @event_payment = sum(a.cont_amt)
from VS_AFFILIATION x
join VS_CONTRIBUTION a on x.group_customer_no = a.customer_no
join VS_CAMPAIGN b on a.campaign_no = b.campaign_no 
where @CUSTOMER_NO = x.individual_customer_no
and x.primary_ind = 'Y'
and b.camp_type = 'e'
and a.cont_dt  between  dateadd(mm, -14, getdate()) and getdate() 
END
/***************************************************/
/*  Determine whether the customer is eligible  */
/*   for free exchanges            */
/**************************************************/
Select @fee_ind = 'N'
From VXS_CONST_CUST a 
Where constituency in (19,21,26,27,28,31,32,33,18,45,47,48) --added 18 and 45 10/11/2007
and (a.end_dt >= getdate() or a.end_dt is null)
and a.customer_no = @customer_no
--affiliates
select  @fee_ind = 'N'
from VS_AFFILIATION a
join VXS_CONST_CUST b on a.individual_customer_no = b.customer_no
where a.primary_ind = 'Y'
and a.group_customer_no = @CUSTOMER_NO
and b.constituency in (19,21,26,27,28,31,32,33,18,45,47,48)
--househould
select  @fee_ind = 'N'
from VS_AFFILIATION a
join VXS_CONST_CUST b on a.group_customer_no = b.customer_no
where a.primary_ind = 'Y'
and a.individual_customer_no = @CUSTOMER_NO
and b.constituency in (19,21,26,27,28,31,32,33,18,45,47,48)
--other affiliate
select  @fee_ind = 'N'
from VS_AFFILIATION a
join VXS_CONST_CUST b on a.individual_customer_no = b.customer_no
where a.primary_ind = 'Y'
and a.individual_customer_no = @CUSTOMER_NO
and b.constituency in (19,21,26,27,28,31,32,33,18,45,47,48)
--primary and secondary contacts
select @fee_ind = 'N'
from VS_RELATIONSHIP a
join VXS_CONST_CUST b on a.customer_no = b.customer_no 
where a.associated_customer_no = @CUSTOMER_NO
and b.constituency in (19,21,26,27,28,31,32,33,18,45,47,48)
and (relationship_type_description like '%primary%' or relationship_type_description like '%secondary%')
and a.inactive = 'N'
/**********************************************/ --08/15/2007 cm
/*    Display 'see CSI!'         */
/*    in header if there is an open    */
/*    Attention CSI    */
/**********************************************/ --06/15/2011 cm
--individuals, groups, households
select @CSI_ind = 'N'
select @CSI_ind = 'Y'
from VS_CUST_ACTIVITY a 
where a.customer_no = @customer_no
and a.activity_type = 152
and not exists(select * from t_issue_action 
where activity_no = a.activity_no
and isnull(res_ind,'N') = 'Y')
-- primary affiliates
select @csi_ind = 'Y'
from VS_AFFILIATION a
join VS_CUST_ACTIVITY b on a.group_customer_no = b.customer_no
where a.primary_ind = 'Y'
and (a.individual_customer_no = @customer_no
OR a.group_customer_no = @CUSTOMER_NO)
and b.activity_type = 152
and not exists(select * from t_issue_action 
where activity_no = b.activity_no
and isnull(res_ind,'N') = 'Y')
/**********************************************/ --08/15/2007 cm
/*    Display 'see Note!'         */
/*    in header if there is a gift entry note   */
/*        */
/**********************************************/ --06/15/2011 cm
--individuals, groups, households
select @see_note = 'N'
select @see_note = 'Y'
from VXS_CUST_NOTES a
where a.customer_no = @customer_no
and a.note_type = 19
/**********************************************/ --07/11/2011 cm
/*    Bravo Circle Rep Name        */
/*          */
/**********************************************/  
select @BCR_rep = ' '
select @BCR_rep = substring(b.key_value,1,8)
FROM      VXS_CUST_KEYWORD B 
WHERE  b.keyword_no = 262 
and @customer_no = b.customer_no 
/**********************************************/ 
/*    Bad Address Indicator       */
/*          */ --9/19/2013 srp
/**********************************************/  
select @bad_add_ind = ' '
--Groups
select @bad_add_ind = 9
FROM VS_AFFILIATION x 
join VS_ADDRESS c on x.group_customer_no = c.customer_no
join VRS_ADDRESS_TYPE b on c.address_type = b.id
where x.primary_ind = 'Y' 
and (x.individual_customer_no = @customer_no
OR x.group_customer_no = @CUSTOMER_NO)
and c.postal_code = '99999' and c.primary_ind = 'Y'
--Indv
select @bad_add_ind = ' '
select @bad_add_ind = 9
FROM      VRS_ADDRESS_TYPE b
join VS_ADDRESS c on b.id = c.address_type
WHERE  c.postal_code = '99999' and c.primary_ind = 'Y'
and c.customer_no = @customer_no  
--Other
select @bad_add_ind = ' '
select @bad_add_ind = 9
FROM      VRS_ADDRESS_TYPE b
join VS_ADDRESS c on b.id = c.address_type
WHERE  c.address_type = 19 and c.primary_ind = 'Y'
and c.customer_no = @customer_no  
/**********************************************/ --12/12/2016 srp
/*    Ask Amt Sub Renewal         */
/*          */
/**********************************************/  
select @ask_amt = 0
select @ask_amt = b.key_value
FROM      VXS_CUST_KEYWORD B 
WHERE  b.keyword_no = 954 
and @customer_no = b.customer_no 
/**********************************************/ 
/*    HOT Prospect Indicator       */
/*          */ --9/19/2013 srp
/**********************************************/  
select @hot_prospect = ' '
--Indv
select @hot_prospect = ' '
select @hot_prospect = 9
FROM VS_PLAN a where
a.priority = 3 and a.campaign_no in (22441,
22442,
22478,
22614)
and a.status in (16, 31, 17, 19, 26, 25, 20, 32)
and a.customer_no = @customer_no  
/************************************************
Portfolio Manager
************************************************/ 
IF @CUSTOMER_NO NOT IN (select worker_customer_no from VXS_CUST_WORKER_TYPE where worker_type=1 and inactive ='N') AND
@CUSTOMER_NO NOT IN (select customer_no from VXS_CONST_CUST where constituency = 71 and end_dt is null)
BEGIN
--on customer
select @primary_manager = (select top 1 dn.display_name_tiny
from VS_RELATIONSHIP a
join FT_CONSTITUENT_DISPLAY_NAME () dn on a.associated_customer_no = dn.customer_no
where a.relationship_type_id in (102,10031,108,10036) and a.inactive='n' 
and a.end_dt is null--primary manager types
--primary manager types
and @CUSTOMER_NO = a.customer_no)
--and @CUSTOMER_NO not in (select worker_customer_no from VXS_CUST_WORKER_TYPE where worker_type=1 and inactive ='N') -- not a worker 
--and @customer_no not in (select customer_no from VXS_CONST_CUST where constituency = 71 and end_dt is null) -- not employee) 
--on household
if @primary_manager is null
begin
select @primary_manager = (select top 1 dn.display_name_tiny
from VS_AFFILIATION x
join VS_RELATIONSHIP a on x.group_customer_no = a.customer_no
join FT_CONSTITUENT_DISPLAY_NAME () dn on a.associated_customer_no = dn.customer_no
where (a.relationship_type_id in (102,10031,108,10036) and a.end_dt is null)
and @CUSTOMER_NO = x.individual_customer_no
and x.primary_ind = 'Y' and a.inactive='N'
and x.name_ind in (-1,-2))
--and @CUSTOMER_NO not in (select worker_customer_no from VXS_CUST_WORKER_TYPE where worker_type=1 and inactive ='N') -- not a worker 
--and @customer_no not in (select customer_no from VXS_CONST_CUST where constituency = 71 and end_dt is null) -- not employee) 
end
--on affilaited individual  
if @primary_manager is null
begin
select @primary_manager = (select top 1 dn.display_name_tiny
from VS_AFFILIATION x
join VS_RELATIONSHIP a on x.individual_customer_no = a.customer_no
join FT_CONSTITUENT_DISPLAY_NAME () dn on a.associated_customer_no = dn.customer_no
where (a.relationship_type_id in (102,10031,108,10036) and a.end_dt is null) --primary manager types
and a.inactive='N'
and @CUSTOMER_NO = x.group_customer_no
and x.primary_ind = 'Y'
and x.name_ind in (-1,-2)
and (X.individual_customer_no not in (select worker_customer_no from VXS_CUST_WORKER_TYPE where worker_type=1 and inactive ='N') -- not a worker 
and X.individual_customer_no not in (select customer_no from VXS_CONST_CUST where constituency = 71 and end_dt is null))) -- not employee)  
end
END
IF @CUSTOMER_NO IN (select worker_customer_no from VXS_CUST_WORKER_TYPE where worker_type=1 and inactive ='N') OR
@CUSTOMER_NO IN (select customer_no from VXS_CONST_CUST where constituency = 71 and end_dt is null) 
BEGIN 
SET @primary_manager=NULL
END
/************************************************
Donor Designation
************************************************/
--find 'multi' customers
/*select customer_no
into #multi
from TX_CUST_KEYWORD
where
keyword_no = 763
and key_value not in ('General
,Indiv./Corp. Challenge Grant
,Operathon Overture Soc Challen
,Merchandise Sponsor
,Operathon-Ind&Corp Challenge G
,Operathon-Chapter Challenge Gr
,Operathon-Merchandise Sponsor
,Overture Challenge Gr-Void
,Name a Seat Campaign Ann.
,Chapters - Young Professional
,Operathon-Lecture Corps
,Young Professionals
,Operathon YP Challenge Grant
,Operathon GB Challenge Grant')
group by customer_no
having COUNT(*) > 1
select @dnr_desig = ''
select @dnr_desig = a.key_value
from VXS_CUST_KEYWORD a
where a.keyword_no = 763
and a.customer_no = @CUSTOMER_NO
and a.customer_no not in (select customer_no from #multi)
select @dnr_desig = 'MULTI'
from #multi 
where customer_no = @CUSTOMER_NO  
*/
 /**********************************************/ --05/07/2019 rp
/*    No Solicitation       */
/*          */
/**********************************************/  
select @no_solicitation = ' '
select @no_solicitation = 'No Solicitation'
FROM      VXS_CUST_KEYWORD B 
WHERE  b.keyword_no = 802 
and @customer_no = b.customer_no  

/**********************************************/ --05/07/2019 rp
/*    No Invites      */
/*                                            */
/**********************************************/  
select @no_invite = ' '
select @no_invite = b.key_value
FROM      VXS_CUST_KEYWORD B 
WHERE  b.keyword_no = 332 and b.key_value = 'Do Not Ever Invite'
and @customer_no = b.customer_no  

 /**********************************************/ --05/07/2019 rp
/*    Next Perf      */
/*                                             */
/**********************************************/  
select @next_perf = ' '

--select @next_perf = (select top 1 CONVERT(varchar,b.perf_dt,1) + ' ' + b.perf_name from VS_TICKET_HISTORY b
-- where perf_dt >= GETDATE()  
--and @customer_no = b.customer_no
--order by b.perf_dt)
; with next_perf as (select customer_no, perf_dt, perf_name, ROW_NUMBER() over (partition by customer_no order by perf_dt) as rownum 
from VS_TICKET_HISTORY where customer_no = @CUSTOMER_NO
and  perf_dt >= GETDATE() )
select @next_perf = CONVERT(varchar,b.perf_dt,1) + ' ' + b.perf_name from next_perf b where rownum = 1 

 /**********************************************/ --05/17/2019 CWH TN ZD#17530
/*    Next Event or Activity      */
/*                                             */
/**********************************************/ 
select @next_event = ''
;
with cust_events (event_dt , description , customer_no ) as (
select 
c.event_dt, 
c.description, 
customer_no
from VXS_EVENT_EXTRACT a 
join VS_CAMPAIGN c on a.campaign_no=c.campaign_no
where a.inv_status in (3,6,7,8,9,17,18)
and customer_no = @customer_no
union
select 
a.sp_act_dt, 
b.description,
 a.customer_no
from VS_SPECIAL_ACTIVITY a 
join VRS_SPECIAL_ACTIVITY b on a.sp_act = b.id
join TR_SPECIAL_ACTIVITY_STATUS c on a.status = c.id
where a.status in (2,6,7,8,10)
and customer_no = @customer_no), 
next_event as (
select ROW_NUMBER() over (partition by customer_no order by event_dt desc) as rownum, 
* from cust_events)

select @next_event =  convert(varchar,event_dt,1) + ' ' + description from next_event
where rownum = 1;

----select @next_event


/**********************************************/
/*      Return the result set      */
/**********************************************/
select customer_no = @customer_no, 
full_name1 = @full_name1,
full_name2 = @full_name2, 
all_const = @all_const, 
memb_level = @memb_level,  current_status = @current_status,
memb_expirtion = @expiration, 
create_dt = @create_dt, 
donation = @donation, 
userid = [dbo].[fs_user](), 
batch_no = null, 
on_account = @on_account,
inactive = CASE WHEN ISNULL(@inactive, 1) > 1 THEN 'INACTIVE' ELSE '' END,
batch_type = null,
designation = @designation,
association = @association,
matching_gifts = @matching_gifts,
o_series = @o_series,
decline_benefits = @decline_benefits,
event_payment = @event_payment,
fee_ind = @fee_ind,
csi_ind = @csi_ind,
bcr_rep = @bcr_rep,
bad_add_ind = @bad_add_ind,
dnr_desig = @dnr_desig,
see_note = @see_note,
primary_manager = @primary_manager,
ask_amt = @ask_amt,
hot_prospect = @hot_prospect,
no_solicitation = @no_solicitation,
no_invite = @no_invite,
next_perf = @next_perf,
next_event = @next_event
end
